function myToggle() {
  var checkbox = document.getElementById("toggle");
  var basic = document.getElementById("basic");
  var professional = document.getElementById("professional");
  var master = document.getElementById("master");

  if (checkbox.checked == true) {
    basic.innerHTML = 19.99;
    professional.innerHTML = 24.99;
    master.innerHTML = 39.99;
  } else {
    basic.innerHTML = 199.99;
    professional.innerHTML = 249.99;
    master.innerHTML = 399.99;
  }
}
